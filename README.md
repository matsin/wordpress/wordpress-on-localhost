# نصب محلی وردپرس

این روش بیشترین شباهت را با محیط استقرار سایت دارد. یعنی تمام عملکردهای وردپرس را می‌توانید در هاست محلی بدست‌آورد.

## پیش‌نیازها

برای استفاده از این مخزن نرم‌افزارهای [داکر](https://docs.docker.com/get-docker/) و [داکرکامپوز](https://docs.docker.com/compose/install/) باید در سیستم‌عاملت نصب باشد.

## روش استفاده

1. رونوشت از منبع: در مسیر دلخواهت این دستور را اجرا کن.

   <div dir="ltr" align="left" markdown="1">

   ```shell
   git clone --depth=1 https://gitlab.com/matsin/wordpress/wordpress-on-localhost.git && rm -rf ./wordpress-on-localhost/.git
   ```

   </div>

   حالا پوشه‌ی پروژه را با یه ویرایشگر باز کن. من از VSCode استفاده می‌کنم.

   <div dir="ltr" align="left" markdown="1">

   ```shell
   cd ./wordpress-on-localhost && code .
   ```

   </div>

2. تعریف متغیرهای محلی: یه رونوشت از `.env-template` به نام `.env` ایجاد و مقدار کلیدهایش را به دلخواهت پرکن. مثال:

   <div dir="ltr" align="left" markdown="1">

   ```text
   # گذرواژه‌ی مدیر پایگاه‌داده
   MYSQL_ROOT_PASSWORD=root_password
   # تعریف یه کاربر پایگاه‌داده
   MYSQL_USER=user_db
   # گذرواژه کاربر جدید
   MYSQL_PASSWORD=password
   # نام پایگاه‌داده
   MYSQL_DATABASE=wordpress
   ```

   </div>

3. اجرای سایت: در ریشه‌ی پروژه دستور زیر را اجرا کن.

   <div dir="ltr" align="left" markdown="1">

   ```shell
   docker-compose up
   ```

   </div>

